import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth.service';
import { UIService } from 'src/app/shared/ui.service';
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit, OnDestroy {

  private hide: boolean = true
  isLoading: boolean;
  private loadingSubs: Subscription;

  constructor(
    private authService: AuthService,
    private uiService: UIService
  ) { }

  ngOnInit() {
    this.loadingSubs = this.uiService.loadingStateChanged.subscribe( isLoading => {
      if (isLoading) {
        this.isLoading = isLoading
      } else {
        this.isLoading = isLoading
      }
    })
  }

  letMeIn(f) {
    this.authService.login({
      email: f.value.email,
      password: f.value.password
    })
  }

  ngOnDestroy() {
    this.loadingSubs.unsubscribe()
  }
}
